using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Dioinecail.MonoBehaviourEditor.Editors
{
    public enum EditorType
    {
        Before,
        After,
        Replace
    }

    public interface ICustomEditor
    {
        EditorType Type { get; }
        void OnEnable(SerializedObject target);
        void OnInspectorGUI(SerializedObject target);
    }

    [CustomEditor(typeof(MonoBehaviour), true)]
    public class CustomMonoBehaviourEditor : Editor
    {
        private ICustomEditor[] m_beforeEditors;
        private ICustomEditor[] m_afterEditors;
        private ICustomEditor[] m_replaceEditors;



        protected virtual void OnEnable()
        {
            CollectEditors();
            OnEnableEditors();
        }

        public override void OnInspectorGUI()
        {
            OnInspectorGUIBefore();

            if (m_replaceEditors != null && m_replaceEditors.Length > 0)
                OnInspectorGUIReplace();
            else
                base.OnInspectorGUI();

            OnInspectorGUIAfter();
        }

        private void CollectEditors()
        {
            var types = TypeCache.GetTypesDerivedFrom(typeof(ICustomEditor));

            var list = new List<ICustomEditor>();

            foreach (var e in types)
            {
                var editor = (ICustomEditor)Activator.CreateInstance(e);
                list.Add(editor);
            }

            m_beforeEditors = list
                .Where(t => t.Type == EditorType.Before)
                .ToArray();

            m_afterEditors = list
                .Where(t => t.Type == EditorType.After)
                .ToArray();

            m_replaceEditors = list
                .Where(t => t.Type == EditorType.Replace)
                .ToArray();
        }

        private void OnEnableEditors()
        {
            foreach (var e in m_beforeEditors)
            {
                e.OnEnable(serializedObject);
            }

            foreach (var e in m_afterEditors)
            {
                e.OnEnable(serializedObject);
            }

            foreach (var e in m_replaceEditors)
            {
                e.OnEnable(serializedObject);
            }
        }

        private void OnInspectorGUIBefore()
        {
            foreach (var e in m_beforeEditors)
            {
                e.OnInspectorGUI(serializedObject);
            }
        }

        private void OnInspectorGUIReplace()
        {
            foreach (var e in m_replaceEditors)
            {
                e.OnInspectorGUI(serializedObject);
            }
        }

        private void OnInspectorGUIAfter()
        {
            foreach (var e in m_afterEditors)
            {
                e.OnInspectorGUI(serializedObject);
            }
        }
    }
}