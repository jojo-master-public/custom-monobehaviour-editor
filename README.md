# Custom MonoBehaviour Editor Utility

A simple custom editor solution to display multiple custom editors for MonoBehaviour class   

## Installation

Open Unity Package Manager  
Click on a plus sign in top left corner  
Choose "Add package from git url.."  
Copy and paste this repository url https://gitlab.com/jojo-master-public/custom-monobehaviour-editor.git    
Hit Add  
Enjoy!  

## Contact

Developed by Andrew Dioinecail.  
Twitter: [Dioinecail](https://twitter.com/dioinecail).  
Email: andrewdionecail@gmail.com  
  
Feel free to ask questions!  